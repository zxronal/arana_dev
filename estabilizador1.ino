/* Sweep
 by BARRAGAN <http://barraganstudio.com>
 This example code is in the public domain.

 modified 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Sweep
*/

#include <Servo.h>

Servo myservo2;
Servo myservo3;
Servo myservo4;
Servo myservo5;
Servo myservo6;
Servo myservo7;
Servo myservo8;
Servo myservo9;

// create servo object to control a servo
// twelve servo objects can be created on most boards

int pos = 0;    // variable to store the servo position
bool isStart;
int i=0;

void setup() {
  
  myservo2.attach(2);
  myservo3.attach(3);
  myservo4.attach(4);  
  myservo5.attach(5);
  myservo6.attach(6);
  myservo7.attach(7);
  myservo8.attach(8);
  myservo9.attach(9);
  // attaches the servo on pin 9 to the servo object
  isStart= true;
  Serial.begin(9600);
}

void start(){
      Serial.print(pos);
      myservo2.write(60);
      delay(200);
      myservo3.write(60);
      delay(200);
      myservo4.write(60);
      delay(200);
      myservo5.write(60);
      delay(200);
      myservo6.write(90);
      delay(200);
      myservo7.write(90);
      delay(200);
      myservo8.write(90);
      delay(200);
      myservo9.write(90);
      delay(200);
      
      isStart = false;
  }
void hello(){
      Serial.print(pos);
      myservo2.write(0);
      delay(200);
      myservo3.write(180);
      delay(200);
      myservo4.write(0);
      delay(200);
      myservo5.write(0);
      delay(200);
      myservo6.write(90);
      delay(200);
      myservo7.write(60);
      delay(200);
      myservo9.write(90);
      delay(200);

      for (i = 0; i < 6; i += 1) {
        
        for (pos = 50; pos <= 140; pos += 1) {
          myservo8.write(pos);
          delay(8);
        }
        
        for (pos = 140; pos >= 50; pos -= 1) {
          myservo8.write(pos);
          delay(8);
        }
        
      }
  delay(200);
  start();
  }

void diagonalone(){
  int pos2= myservo6.read();
  Serial.print(pos2);//90
 
  for (pos = myservo8.read(); pos <= 140; pos += 1) {
   
   if(pos2>=40){
    myservo9.write(pos2);
    myservo6.write(pos2);
    delay(15);
    pos2 -= 1;
   }
     myservo7.write(pos);
     myservo8.write(pos);
     delay(15);
   }
  start();
}


void rotar(){
myservo2.write(140);
myservo4.write(140);

myservo5.write(140);
myservo3.write(140);
myservo2.write(60);
myservo4.write(60);

delay(110);
myservo6.write(150);
myservo8.write(160);
delay(150);
myservo2.write(140);
myservo4.write(140);
delay(110);

//diagonal1}

myservo5.write(60);
myservo3.write(60);
delay(110);
myservo7.write(160);
myservo9.write(160);
delay(150);
myservo5.write(140);
myservo3.write(140);
delay(110);
//devulvis


myservo2.write(60);
myservo4.write(60);
delay(110);
myservo7.write(90);
myservo9.write(90);
delay(110);

myservo2.write(140);
myservo4.write(140);
delay(110);
myservo5.write(60);
myservo3.write(60);

delay(110);
myservo6.write(80);
myservo8.write(90);
delay(150);
myservo5.write(140);
myservo3.write(140);

delay(110);

//myservo7.write(90);
//myservo9.write(90);
}

void dance(){
    for (pos = 0; pos <= 140; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo6.write(pos);
    myservo7.write(pos);
    myservo8.write(pos);
    myservo9.write(pos);
    // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  for (pos = 140; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
    myservo6.write(pos);
    myservo7.write(pos);
    myservo8.write(pos);
    myservo9.write(pos);
    // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }  
}



void loop() {

Serial.println("is start ");
 if (isStart)
  {
    Serial.println("is start work");
    start();
    delay(200);
    hello();
    delay(200);
  }
  //dance();
  //delay(1000);
  //diagonalone();
  rotar();
  delay(200);
  
}
